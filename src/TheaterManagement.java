import java.util.Arrays;

public class TheaterManagement {
	private final String rollChar[] = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O"};
	Date mon,tue,wed,thu,fri,sat,sun;
	public TheaterManagement(){
		mon = new Date("mon");
		tue = new Date("tue");
		wed = new Date("wed");
		thu = new Date("thu");
		fri = new Date("fri");
		sat = new Date("sat");
		sun = new Date("sun");
		mon.createShowTime1(DataSeatPrice.ticketPrices);
		tue.createShowTime1(DataSeatPrice.ticketPrices);
		wed.createShowTime1(DataSeatPrice.ticketPrices);
		thu.createShowTime1(DataSeatPrice.ticketPrices);
		fri.createShowTime1(DataSeatPrice.ticketPrices);
		sat.createShowTime2(DataSeatPrice.ticketPrices);
		sun.createShowTime2(DataSeatPrice.ticketPrices);
			
	}

	public int getRollInt(String roll){
		int rollInt = (Arrays.binarySearch(rollChar,roll.toUpperCase()))+1;
		return rollInt;
	}
	public double reserveSeat(String d,int show,String roll,int seat){
		double[][] showtime = null;
		if(d.equals("mon")){
			if(show==1){
				showtime =mon.showtime1;
			}
			else if(show==2){
				showtime =mon.showtime2;
			}
		}
		else if(d.equals("tue")){
				if(show==1){
				showtime =tue.showtime1;
				}
				else if(show==2){
					showtime =tue.showtime2;
				}
			
		}
		else if(d.equals("wed")){
			if(show==1){
			showtime =wed.showtime1;
			}
			else if(show==2){
				showtime =wed.showtime2;
			}
		
		}
		else if(d.equals("thu")){
			if(show==1){
			showtime =thu.showtime1;
			}
			else if(show==2){
				showtime =thu.showtime2;
			}
		
		}
		else if(d.equals("fri")){
			if(show==1){
			showtime =fri.showtime1;
			}
			else if(show==2){
				showtime =fri.showtime2;
			}
		}
		else if(d.equals("sat")){
			if(show==1){
			showtime =sat.showtime1;
			}
			else if(show==2){
				showtime =sat.showtime2;
			}
			else if(show==3){
				showtime =sat.showtime3;
			}
		}
		else if(d.equals("sun")){
			if(show==1){
			showtime =sun.showtime1;
			}
			else if(show==2){
				showtime =sun.showtime2;
			}
			else if(show==3){
				showtime =sun.showtime3;
			}
		}
		seat = seat-1;
		int rollInt = getRollInt(roll);
		double prices = -1;
		if(showtime[rollInt][seat] > 0){
			prices = showtime[rollInt][seat];
			showtime[rollInt][seat] = 0;
		}
		else if(showtime[rollInt][seat] == 0){
			prices = 0;
		}
		return prices;
	}
	public void showAllSeat(String d,int show){
		
		int roll = 13;
		int seat = 20;
		double[][] showtime = null;
		if(d.equals("mon")){
			if(show==1){
				showtime =mon.showtime1;
			}
			else if(show==2){
				showtime =mon.showtime2;
			}
		}
		else if(d.equals("tue")){
				if(show==1){
				showtime =tue.showtime1;
				}
				else if(show==2){
					showtime =tue.showtime2;
				}
			
		}
		else if(d.equals("wed")){
			if(show==1){
			showtime =wed.showtime1;
			}
			else if(show==2){
				showtime =wed.showtime2;
			}
		
		}
		else if(d.equals("thu")){
			if(show==1){
			showtime =thu.showtime1;
			}
			else if(show==2){
				showtime =thu.showtime2;
			}
		
		}
		else if(d.equals("fri")){
			if(show==1){
			showtime =fri.showtime1;
			}
			else if(show==2){
				showtime =fri.showtime2;
			}
		}
		else if(d.equals("sat")){
			if(show==1){
			showtime =sat.showtime1;
			}
			else if(show==2){
				showtime =sat.showtime2;
			}
			else if(show==3){
				showtime =sat.showtime3;
			}
		}
		else if(d.equals("sun")){
			if(show==1){
			showtime =sun.showtime1;
			}
			else if(show==2){
				showtime =sun.showtime2;
			}
			else if(show==3){
				showtime =sun.showtime3;
			}
		}
		System.out.println(showtime[1][3]);
		for(int i=0;i<roll;i++){
			for(int j=0;j<seat;j++){
			if(showtime[i][j]>0){
				System.out.print(rollChar[i]+j+" ");
			}
			}
			System.out.println("");
		}
	}

}
